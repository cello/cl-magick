;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-magick; -*-
;;;
;;; Copyright � 2004 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(in-package :cl-magick)

(defclass wand-image ()
  ((direction :initarg :direction :initform :input :accessor direction)
   (file-path$ :initarg :file-path$ :accessor file-path$ :initform nil)
   (mgk-wand :initarg :mgk-wand :accessor mgk-wand :initform nil)
   (image-size :initarg :image-size :accessor image-size :initform nil)
   (tile-p :initarg :tile-p :accessor tile-p :initform t)
   ))

(defmethod initialize-instance :after ((self wand-image) &key)
  (ecase (direction self)
    (:output (progn
               (assert (pixels self))
               (assert (image-size self))
               (setf (mgk-wand self) (new-magick-wand))
               (destructuring-bind (columns . rows) (image-size self)
                 (uffi:with-cstring (rgbc "CRGB")
                   (assert (zerop (magick-set-image-pixels
                                   (setf (mgk-wand self) (new-magick-wand))
                                   0 0 columns rows rgbc 3 (pixels self)))))
                 (magick-set-image-type (mgk-wand self) 3)
                 )))
    (:input
     (assert (probe-file (file-path$ self)) ()
       "Image file ~a not found initializing wand" (file-path$ self))
     (assert (not (mgk-wand self))) ;; make sure not leaking
     (setf (mgk-wand self) (path-to-wand (file-path$ self)))
     ;(mgk-wand-dump (mgk-wand self) (file-path$ self))
     (when (and (mgk-wand self) (not (image-size self)))
       (setf (image-size self)
         (cons (magick-get-image-width (mgk-wand self))
           (magick-get-image-height (mgk-wand self))))))))

(defmethod wand-release ((wand wand-image))
  (when (mgk-wand wand)
    ;(print (list "destroying magick wand" wand))
    ;(describe wand)
    (destroy-magick-wand (mgk-wand wand))
    ))

(defun path-to-wand (path)
  (let ((wand (new-magick-wand))
        (p (namestring path)))
    (assert (probe-file p))
    (uffi:with-cstring (cpath p)
      (let ((stat (magick-read-image wand cpath)))
        (if (zerop stat)
            (format t "~&magick-read jpeg failed on ~a" p)
          (format t "~&magick-read-OK ~a" p)))
      wand)))

#+not
(defun path-to-wand (path)
  (let ((wand (new-magick-wand))
        (p (namestring path)))
    (assert (probe-file p))
    (uffi:with-cstring (cpath p)
      (unless (zerop (magick-read-image wand cpath))
        (break "magick-read jpeg failed on ~a" p))
      wand)))

(defparameter *mgk-columns*
  (fgn-alloc :unsigned-long 1 :ignore))

(defparameter *mgk-rows*
  (fgn-alloc :unsigned-long 1 :ignore))

(defun wand-image-size (wand)
  (magick-get-size wand *mgk-columns* *mgk-rows*)
  (cons (ff-elt *mgk-columns* :unsigned-long 0)
    (ff-elt *mgk-rows* :unsigned-long 0)))

(defun wand-get-image-pixels (wand 
                                &optional (first-col 0) (first-row 0)
                                (last-col (magick-get-image-width wand))
                                (last-row (magick-get-image-height wand)))
    (let* ((columns (- last-col first-col))
           (rows (- last-row first-row))
           (pixels (fgn-alloc :unsigned-char (* 3 columns rows) :wand-image)))
      ;;(print (list "wand-get-image-pixels got" wand (* 3 columns rows) pixels))
      (uffi:with-cstring (rgbc "RGB")
        (magick-get-image-pixels wand first-col first-row columns rows rgbc 0 pixels ))
      #+testing (progn
                  (incf testn)
                  (uffi:with-cstring (cpath (format nil "C:\\TEST~a.JPG" testn)) ;; p)
                    (print `(writeimage ,(magick-write-image wand cpath))))
                  (uffi:with-cstring (cpath (format nil "C:\\TEST~a.GIF" testn)) ;; p)
                    (print `(writeimage ,(magick-write-image wand cpath))))
                  #+not (uffi:with-cstring (cpath "C:\\TEST.BMP") ;; p)
                          (print `(writeimage ,(magick-write-image wand cpath)))))
      
      (values pixels columns rows)))

