;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-magick; -*-
;;;
;;; Copyright � 1995,2003 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.


(in-package :cl-magick)

(defun mgk-wand-dump (w &rest info)
  (declare (ignorable w))
  #+not (trc "mgk-wand-dump" w info)
  ;; (ukt::trc "> width"  (magick-get-image-width w))
  ;; (ukt::trc "> height"  (magick-get-image-height w))
  ;; (ukt::trc "> description" (magick-describe-image w))
  )
  
(defconstant wcx 640)        ;; Window Width
(defconstant wcy 480)        ;; Window Height
(defparameter xrot 0.0f0)
(defparameter yrot 0.0f0)
(defparameter zrot 0.0f0)

(ff-defun-callable :cdecl :void r6wffx ()
  (r6w))

(defparameter *skin6* nil)

#+test
(progn
  (defun makin-movies ()
    ;;(cl-magick-init)
    (let* ((w (path-to-wand (test-templates "metal.gif"))))
      (loop for jpeg in (directory "c:/dvx-images/out/snap-me*.jpg")
          do (let ((c (path-to-wand (namestring jpeg))))
               (assert c)
               (magick-reset-iterator w)
               (print `(adding-image ,(magick-add-image w c) ,(pathname jpeg)))
               (destroy-magick-wand c)))

      (magick-reset-iterator w)
      (loop until (zerop (magick-has-next-image w))
          for n upfrom 0
          do (print (list n (magick-get-image-index w)
                      (magick-get-image-filename w)
                      (magick-get-image w)))
            (print `(next-image ,(magick-next-image w)))
            )
      (magick-reset-iterator w)
      (print `(write-images 
               ,(magick-write-images w (cl-user::path-to-temp "a.gif") 0)))
      (destroy-magick-wand w)))
  (makin-movies))


#+test
(progn
  (cl-magick-init)
  (path-to-wand "c:/dvx-images/templates/metal.gif"))


#+test
(progn
  (defun witerate ()
    (print (magick-get-copyright ))
    (print (magick-get-version *mgk-version*))
    (let* ((w (path-to-wand "c:/dvx-images/templates/metal.gif"))
           ;(w (new-magick-wand))
           (odd (path-to-wand (test-shapers "moon.jpg")))
           (even (path-to-wand (test-shapers "grace.jpg")))
           (third (path-to-wand (test-shapers "slvrsilk.gif")))
           
           )
      #+not (print (uffi:with-cstring (test "test.mpg")
               (magick-set-image-filename w test)))
      ;(print (magick-get-image metal))
      ;(print (magick-get-image w))
      ;(print `(removing ,(magick-remove-image w)))
      ;(print (magick-reset-iterator w))
      ;(print (list :test (magick-has-next-image w)(magick-get-image w)))
      (loop for n below 3
            for new = (ecase (mod n 3)
                        (0 even)
                        (1 odd)
                        (2 third))
          do (print (magick-reset-iterator w))
            #+not (loop until (zerop (magick-has-next-image w))
                      do (print `(next-image ,(magick-next-image w)))
                      finally (print (list 'pre-add-index (magick-get-image-index w))))
            (print `(adding-image ,(magick-get-image-filename new)))
            (print `(adding-image ,(magick-add-image w new)))
            (print (list 'post-add-index (magick-get-image-index w)))
            (print `(post-add-filename ,(magick-get-image-filename w)))
            #+not (when whack-1
                    (assert (eql 1 (magick-get-image-index w)))
                    (print `(whacking ,(magick-get-image-filename w)))
                    (magick-remove-image w))
            ;;;            (print (magick-get-image w))
            ;;;            (loop repeat 2
            ;;;                  do (print `(next-image ,(magick-next-image w)))
            ;;;                  (print (list 'loop-image-index (magick-get-image-index w))))
            )
      (print `(reset ,(magick-reset-iterator w)))
      (print `start-file-dump--------------------)
      (loop until (zerop (magick-has-next-image w))
          for n upfrom 0
          do (print (list 'loop-image-index (zerop (magick-has-next-image w))
                      (magick-get-image-index w)))
            ;;(print (list 'image n (magick-get-image w)))
            (print `(next-imagex ,(magick-next-image w)))
            (print (list 'image-filename (magick-get-image-filename w)))
            ;(print `(delay ,(magick-set-image-delay w 200)))
            ;;(print `(next-image ,(magick-next-image w)))
            ;(print (list 'image-index (magick-get-image-index w)))
            ;(print `(delay ,(magick-set-image-delay w 200)))
            ;(print (magick-next-image w))
            )
      ;(print (magick-reset-iterator w))
      ;(wand-images-write w "c:/dvx-images/out/ani.mov" 1)
      (values)))
  (witerate))

#+test
(defun wandorig ()
  (wand-orign 0)
  #+not (loop for n upto sinc-filter
        do (unless nil (find n '(2 3 4 5 6)) ;;(2 3 4 5 6))
             (wand-orign n))))

#+test
(defun wand-orign (n)
  (print `(wand-orig ,n))
  (xim)
  (cl-magick-init)
  (let ((old (magick-wand-template))
        (moon (path-to-wand (cl-user::path-to-temp "bingo0.jpg"))))
    ;(mgk-wand-dump old "existing gif")
    ;(mgk-wand-dump moon "existing gif")
    (let ((new (clone-magick-wand old))
          (pixels (wand-get-image-pixels moon)))
;;;      (print `(old-format ,(magick-get-image-format old)))
;;;      (print `(old-type ,(magick-get-image-type old)))
;;;      (print `(dims-old ,(cons (magick-get-image-width old)(magick-get-image-height old))))
;;;      (print `(dims-moon ,(cons (magick-get-image-width moon)(magick-get-image-height moon))))
;;;      (print `(set name ,(magick-set-filename new "c:\\images\\moon.gif")))
;;;      (print `(new-format ,(magick-get-image-format new)))
;;;      ;(print `(set-type ,(magick-set-image-type new (magick-get-image-type moon))))
;;;      (print `(new-type ,(magick-get-image-type new)))
      (print `(resizetomoon ,n ,(let ((w (magick-get-image-width moon))
                                   (h (magick-get-image-height moon)))
                               (time (magick-resize-image new w h
                                       n ;; gaussian-filter
                                       0)))))
      #+not (print `(set-size-new ,(magick-set-size new
                                     (magick-get-image-width moon)
                                     (magick-get-image-height moon))))
      ;;(print `(dims-new ,(cons (magick-get-image-width new)(magick-get-image-height new))))
      (unwind-protect
          (uffi:with-cstring (rgbc "RGB")
            
            (if (zerop (magick-set-image-pixels new 0 0 
                         (magick-get-image-width moon)
                         (magick-get-image-height moon)
                         rgbc
                         short-pixel
                         pixels))
                (print `(bad pixel set at ,n))
              (progn
                (print `(shrinkimage ,(let ((w (magick-get-image-width moon))
                                            (h (magick-get-image-height moon)))
                                        (time (magick-resize-image new
                                                (round w 2)
                                                (round h 2)
                                                gaussian-filter
                                                0)))))
                
                #+not (print `(type ,(magick-set-image-type new
                                       (magick-get-image-type old)))) ;; Pallette
                ;(print `(depth ,(magick-set-image-depth new (magick-get-image-depth old))))
                ;;(mgk-wand-dump new "under construction")
                (uffi:with-cstring (c (format nil "c:\\images\\stonex-~d.jpg" n))
                  (print `(straight-write-new ,(magick-write-image new c))))
                )))
        (destroy-magick-wand old)
        (destroy-magick-wand moon)
        (destroy-magick-wand new)
        )
      )))
  
(defvar *grace*)
  
(defun r6w ()
  (gl-load-identity)
  (gl-clear (+ gl_color_buffer_bit gl_depth_buffer_bit))
  ;--------------------------------------------
  
  (progn
    (gl-translatef 0 0 -5) 
    
    (gl-rotatef (incf xrot 0.3) 1.0 0.0 0.0)
    (gl-rotatef (incf yrot 0.2) 0.0 1.0 0.0)
    (gl-rotatef (incf zrot 0.4) 0.0 0.0 1.0)
    
    (wand-texture-activate *skin6*)
    
    (flet ((v3f (x y z)
             (let ((scale 1))
               (gl-vertex3f (* scale x)(* scale y)(* scale z)))))
      (with-gl-begun (gl_quads)
        ;; Front Face
        (gl-tex-coord2f 0 0)(v3f -1 -1  1)
        (gl-tex-coord2f 1 0)(v3f  1 -1  1)
        (gl-tex-coord2f 1 1)(v3f  1  1  1)
        (gl-tex-coord2f 0 1)(v3f -1  1  1)
        
        ;; Back Face
        (gl-tex-coord2f 1 0) (v3f -1 -1 -1)
        (gl-tex-coord2f 1 1) (v3f -1  1 -1)
        (gl-tex-coord2f 0 1) (v3f  1  1 -1)
        (gl-tex-coord2f 0 0) (v3f  1 -1 -1)
        ;;;   Top Face
        (gl-tex-coord2f 0 1) (v3f -1  1 -1)
        (gl-tex-coord2f 0 0) (v3f -1  1  1)
        (gl-tex-coord2f 1 0) (v3f  1  1  1)
        (gl-tex-coord2f 1 1) (v3f  1  1 -1)
        ;;;   Bottom Face
        (gl-tex-coord2f 1 1) (v3f -1 -1 -1)
        (gl-tex-coord2f 0 1) (v3f  1 -1 -1)
        (gl-tex-coord2f 0 0) (v3f  1 -1  1)
        (gl-tex-coord2f 1 0) (v3f -1 -1  1)
        ;;;   Right face
        (gl-tex-coord2f 1 0) (v3f  1 -1 -1)
        (gl-tex-coord2f 1 1) (v3f  1  1 -1)
        (gl-tex-coord2f 0 1) (v3f  1  1  1)
        (gl-tex-coord2f 0 0) (v3f  1 -1  1)
        ;;;   Left Face
        (gl-tex-coord2f 0 0) (v3f -1 -1 -1)
        (gl-tex-coord2f 1 0) (v3f -1 -1  1)
        (gl-tex-coord2f 1 1) (v3f -1  1  1)
        (gl-tex-coord2f 0 1) (v3f -1  1 -1)
        ))
    ;;(wand-render *grace* 0 0 1 -1)
    )
  (glut-swap-buffers)
  (glut-post-redisplay)
  )

(defun test-image (filename filetype)
  (make-pathname
    :directory '(:absolute "0dev" "user" "graphics" "shapers")
    :name (string filename)
    :type (string filetype)))

(defun r6init()
  (gl-enable gl_texture_2d)
  (gl-shade-model gl_smooth)
  (gl-clear-color 0 0 0 1)
  (gl-clear-depth 1)
  (gl-enable gl_depth_test)
  (gl-depth-func gl_lequal)
  (gl-hint gl_perspective_correction_hint gl_nicest)
  (setf *skin6* (mgk:wand-ensure-typed 'wand-texture
                  (test-image "jmcbw512" "jpg")))
  (setf *grace* (mgk:wand-ensure-typed 'wand-pixels
                  (test-image "grace" "jpg"))))


#+test
(lesson-6)

(ff-defun-callable :cdecl :void r6-reshape ((x :int)(y :int))
  (r6reshape x y))

(defun r6reshape (width height)
  (unless (or (zerop width) (zerop height))
    (gl-viewport 0 0 width height)
    (gl-matrix-mode gl_projection)
    (gl-load-identity)
    (glu-perspective 45 (/ width height) 0.1 100)
    (gl-matrix-mode gl_modelview)
    (gl-load-identity)))

(defun cl-magick-test ()
  (let ((ogl::*gl-begun* nil))
    (wands-clear)
    (setf *skin6* nil)
    
    (cl-glut-init)
    (glut-set-option glut_action_on_window_close glut_action_glutmainloop_returns)
    
    (glut-init-display-mode (+ glut_rgb glut_double)) ;; Display Mode (Rgb And Double Buffered)
    (glut-init-window-size wcx wcy)   ;; Window Size If We Start In Windowed Mode
    
    (let ((key "NeHe's OpenGL Framework"))
      (uffi:with-cstring (key-native key)
        (glut-create-window key-native)))
    
    (r6init)
    (r6reshape wcx wcy)
    
    (glut-display-func (ff-register-callable 'r6wffx))
    (glut-reshape-func (ff-register-callable 'r6-reshape))
    (glut-keyboard-func (ff-register-callable 'mgwkey))
    (glutmainloop)))

#+test
(cl-magick-test)

(ff-defun-callable :cdecl :void mgwkey ((k :int)(x :int)(y :int))
  (mgwkeyi k x y))

(defun mgwkeyi (k x y)
  (declare (ignorable k x y))
  (print (list "mgwkey" k x y (glutgetwindow)))
  (let ((mods (glut-get-modifiers)))
    (declare (ignorable mods))
    (print (list :keyboard k mods x  y (code-char (logand k #xff))#+not(glut-get-window)))
    (case (code-char (logand k #xff))
      (#\escape (progn
                  (print (list "destroying window" (glutgetwindow) )
                    )
                  (glut-destroy-window (glutgetwindow)))))))