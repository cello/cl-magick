;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-magick; -*-
;;;
;;; Copyright � 2004 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(defpackage :cl-magick
    (:nicknames :mgk)
    (:use
     #:common-lisp
     #-(or cormanlisp ccl) #:clos
     #:hello-c
     #+cl-opengl
     #:cl-opengl ;; wands as opengl textures
     )
  (:export #:wand-manager #:wand-ensure-typed
    #:wands-clear #:wand-pixels #:wand-texture 
    #:wand-render
    #:image-size #:wand-texture-activate #:xim
    #:magick-get-image-width #:magick-get-image-height #:magick-get-image-pixels
    #:new-magick-wand #:magick-read-image #:magick-flip-image #:wand-get-image-pixels
    #:path-to-wand #:mgk-wand-images-write
    #:magick-wand-template))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew :cl-magick *features*))

(in-package :cl-magick)

(defun magick-wand-template ()
  (path-to-wand
   (make-pathname
     :directory '(:absolute "0dev" "user"
                   "graphics" "templates")
     :name "metal" :type "gif")))

(defparameter *imagick-dll-loaded* nil)
(defparameter *wands-loaded* nil)

(defparameter *mgk-version* (fgn-alloc :unsigned-long 1 :ignore))

;-------------------------------------------------------------------

(defun cl-magick-init ()
  (unless *imagick-dll-loaded*
    (assert (probe-file *magick-dynamic-lib*))
    (print "clearing magick wands")
    (wands-clear)
    (print `(loading shared library ,*magick-dynamic-lib*))
    (assert (setq *imagick-dll-loaded*
                  (uffi:load-foreign-library *magick-dynamic-lib*
                    :force-load #+lispworks nil #-lispworks t
                    :module "imagick"))
      () "Unable to load imagick from: ~a" *magick-dynamic-lib* )
    (print (magick-get-copyright ))
    (print (magick-get-version *mgk-version*))
    ))

(defun wands-loaded () *wands-loaded*)
(DEFUN (setf wands-loaded) (new-value)
  (setf *wands-loaded* new-value))

(defun wands-clear ()
  (loop for wand in *wands-loaded*
        do (wand-release (cdr wand)))
  (setf *wands-loaded* nil))

(defun wand-ensure-typed (wand-type file-path$ &rest iargs)
  (when file-path$
    (cl-magick-init)
    (let ((key (list* wand-type (namestring file-path$) iargs)))
      (or (let ((old (cdr (assoc key (wands-loaded) :test 'equal)))) ;;/// primitive test
            #+shhh (when old
                     (print `(wand-ensure-typed re-using prior load ,wand-type ,file-path$)))
            old)
        (let ((wi (apply 'make-instance wand-type
                    :file-path$ file-path$
                    iargs)))
          #+shhh (print `(wand-ensure-typed forced to load ,wand-type ,file-path$))
          (push (cons key wi) (wands-loaded))
          wi)
        (error "Unable to load image file ~a" file-path$)))))

#+allegro
(defun xim ()
  (wands-clear)
  (dolist (dll (ff:list-all-foreign-libraries))
    (when (search "wand" (pathname-name dll))
      (print `(unloading foreign library ,dll))
      (setf *imagick-dll-loaded* nil)
      (ff:unload-foreign-library dll))))

